/*****************************************************/
/* --- Делаем движение машины более реалистичным --- */
/*****************************************************/

CSSRuleList.prototype.findKeyframes = function(name) // находит keyframes по названию анимации
{
  for (var i=0; i<this.length; i++)
  {
    if (this[i].type==CSSRule.KEYFRAMES_RULE && this[i].name==name) return this[i];
  }
  return null;
};

function setWheelsRotationAngle(angle) // устанавливает угол поворота колёс
{
  var rules = document.styleSheets[0].cssRules;
  var rule = rules.findKeyframes('wheel');
  if (rule) rule.cssRules[1].style.setProperty('transform','rotate('+angle+'deg)');
  rule = rules.findKeyframes('wheel-back');
  if (rule)
  {
    rule.cssRules[0].style.setProperty('transform','rotate('+angle+'deg)');
    rule.cssRules[1].style.setProperty('transform','rotate('+Math.floor(angle/2)+'deg)');
  }
}

function updateWheelsRotationAngle() // вычисляет угол поворота колёс исходя из длины траектории и диаметра колеса
{
  var
    w = document.body.clientWidth - 307,
    c = 42 * Math.PI,
    a = Math.floor(w / c * 360);
  setWheelsRotationAngle(a);
}

window.onresize = updateWheelsRotationAngle;

updateWheelsRotationAngle();

var
  car = document.getElementsByClassName('car')[0],
  wheels = document.getElementsByClassName('wheel');

// Переключаемся на другую анимацию чтобы осуществить плавную остановку машины и плавный старт задним ходом.
// При использовании одной анимации и animation-iteration-count: 1.5 машина меняет напрвление движения резко, не сбавляя скорости.
function onAnimationEnded()
{
  car.classList.add('car-back');
  for (var i=0; i<wheels.length; i++) wheels[i].classList.add('wheel-back');
}

car.addEventListener('animationend',onAnimationEnded);
car.addEventListener('webkitAnimationEnd',onAnimationEnded);